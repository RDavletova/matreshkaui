const MatreshkaMyStructurePage = function(){ 

  const menuItemMyStructure = ('a[href="/cabinet/structure"]');
  const searchField = ('[placeholder="Поиск по имени, номеру телефона и email"]');
  
  
  this.getRefferalName = async function (){ // получаем  локатор элемента
    const refferalName = ('[class="gl-table-cell structure-cell name-cell"]');
    return refferalName;
  };

  this.getRefferalPhone = async function (){ // получаем  локатор элемента
    const refferalName = ('[class="gl-table-cell structure-cell phone-cell"]');
    return refferalName;
  }

  
  this.getRefferalEmail = async function (){
    const refferalEmail = ('[class="gl-table-cell structure-cell email-cell"]');
    return refferalEmail;
  }



  this.searchByNameRefferal = async function (page, username){ // поиск по имени реферала
      
    await page.waitForSelector(menuItemMyStructure) 
          .then(
            () => console.log('Загрузился элемент'),
          ); 

    await page.click(menuItemMyStructure);

    await page.waitForSelector(searchField) 
        .then(
          () => console.log('Загрузился элемент'),
        );
        
    await page.fill(searchField, username);
  };


  


  this.searchByPhoneRefferal = async function (page, phone){ //поиск по номеру  реферала
  
    await page.waitForSelector(menuItemMyStructure) 
        .then(
          () => console.log('Загрузился элемент'),
        ); 

    await page.click(menuItemMyStructure);

    await page.fill(searchField, phone);
  };


  this.searchByEmailRefferal = async function (page, email){ // поиск по email реферала

    await page.waitForSelector(menuItemMyStructure) 
          .then(
            () => console.log('Загрузился элемент'),
          ); 

    await page.click(menuItemMyStructure);
    
    await page.fill(searchField, email); 
  };



};

export { MatreshkaMyStructurePage };