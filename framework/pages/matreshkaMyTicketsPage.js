const MatreshkaMyTisketsPage = function(){ 

  const MenuItemMyTicket = ('a[href="/cabinet/tickets"]');
  const toggler = ('[class="toggler"]');
  const ticketNum = ('[class="ticket-num"]');
  const searchField = ('[placeholder="Поиск по номеру билета"]'); //поле поиска

  const purchaseButton = ('a[href="/cabinet/purchase"]'); // кнопка Купить еще
  const countTicktesField = ('input[inputmode="numeric"]');
 
  const buyTicketButton = ('[class="gl-btn mod-lg mod-blue gl-full-width"]'); //синяя кнопка Купить
  const onlinePaymentButton = ('img[src="/assets/images/icons/payment/online-payment.svg"]');
  const partlyPaymentbField = ('div.partly-pay-field > app-form-item > div.field > label > input'); 
  
  

  this.getTicketNumberElem = async function (){ // получение эл-та по локатору
    const ticketNumberElem = ('[class="ticket-num"]');
    return ticketNumberElem;
  };


  this.getcommonCountTickets = async function (){
    const commonCountTickets = ('[class="outcome-grid-value total-tickets"]');
    return commonCountTickets;
  }


  this.getPriceEnot = async function (){
    const priceEnot = ('[class="header-block__main-price"]'); // на странице оплаты тестовая стоиомость билета
    return priceEnot;
  };


  this.getcountTicketsInModal = async function (){
    const countTicketsInModal = ('[class="gl-modal-info-block-value"]'); // надпись на модалке
    return countTicketsInModal;
  };

  
  this.SearchByTicketNumber = async function (page, ticketNumber){ 

      await page.waitForSelector(MenuItemMyTicket)
      .then(
          () => page.click(MenuItemMyTicket), 
      );

      await page.waitForSelector(searchFieldForTickets)
      .then(
          () => console.log('поле поиска подгрузилось'), 
      );
      
      await page.fill(searchFieldForTickets, ticketNumber);
  };




  this.FilterByTicketWinner = async function (page){  // фильтрация по выигрышным билетам

    await page.waitForSelector(MenuItemMyTicket) 
          .then(
            () => console.log('Загрузился элемент'),
          ); 

    await page.click(MenuItemMyTicket);
    
    await page.waitForSelector(toggler) 
          .then(
            () => console.log('Загрузился элемент'),
          ); 

    await page.click(toggler); // жмем на тогглер 

    await page.waitForSelector(ticketNum) 
          .then(
            () => console.log('Загрузился элемент'),
          ); 
  };



  this.BuyTicketWithWallet = async function (page, ticketCount){ // покупка кошльком матрешки

    await page.click(purchaseButton);

    await page.fill(countTicktesField , ticketCount);

    await page.click(buyTicketButton);

  };
  


  this.BuyTicketWithCard = async function (page, ticketCount){ // покупка картой

    await page.click(purchaseButton);

    await page.fill(countTicktesField , ticketCount);

    await page.click(onlinePaymentButton); 
    
    await page.fill(partlyPaymentbField, '0'); 
  
    await page.click(buyTicketButton);
  };


};

export { MatreshkaMyTisketsPage };