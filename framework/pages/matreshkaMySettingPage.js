const MatreshkaMySettingPage = function() { 

  const recoveryLink = ('a[href="/auth/recovery"]'); //ссылка Забыли пароль
  const searchField = ('input[type="text"]'); // поле ввода
  const recoveryButton = ('button[type="button"]'); // кнопка Dосстаноить пароль
  

  
  const settingsLink = ('a[href="/cabinet/settings"]');
  const changePasswordLink = ('a[href="/cabinet/settings/change-password"]');
  const OldPasswordField  = ('input[placeholder="Введите пароль"]');
  const NewPasswordField  = ('input[placeholder="Новый пароль"]');
  const buttonSubmit = ('button[type="submit"]'); // кнопка Изменить
  const username = ('.user-name');
  const logOutButton = ('[class="gl-options-menu-list-item logout-btn"]');
  const loginButton = ('a[href="/auth/login"]');



  this.getPasswordRecoweryLabel = async function (){
    const recoveryLabel = ('[class="gl-auth-card-title"]'); // надпись на модалке "Восстановление пароля"
    return recoveryLabel;
  };

  this.recoveryPassword = async function (page, email){ 
    
    await page.click(recoveryLink); 
    
    await page.fill(searchField, email); 
    
    await page.click(recoveryButton); 

    await page.$('text = "Восстановление пароля"'); 

    //await page.$('text = "Войдите"');//Текст на старнице
    
  };

  this.updatePassword = async function (page, password, newPassword){ // смена пароля
      
        await page.click(settingsLink);

        await page.click(changePasswordLink);
        
        await page.fill(OldPasswordField, password);

        await page.fill(NewPasswordField, newPassword);

        await page.click(buttonSubmit);

        await page.click(username);

        await page.click(logOutButton);

        await page.click(loginButton);
    
  };

};

export { MatreshkaMySettingPage };