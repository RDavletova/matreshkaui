import { MatreshkaMainPage } from './matreshkaMainPage';
import { MatreshkaProfilePage } from './matreshkaProfilePage'; 
import { MatreshkaMyTisketsPage } from './matreshkaMyTicketsPage';
import { MatreshkaMyStructurePage } from './matreshkaMyStructurePage';
import { MatreshkaMySettingPage } from './matreshkaMySettingPage'; 
import { Data } from './data';

const url = 'https://matreshka.technaxis.com/auth/login';

const app = () => ({  
    MatreshkaMyStructurePage: ()  => new MatreshkaMyStructurePage(),
    MatreshkaMainPage: () => new MatreshkaMainPage(),
    MatreshkaProfilePage: () => new MatreshkaProfilePage(),
    MatreshkaMyTisketsPage: () => new MatreshkaMyTisketsPage(),
    MatreshkaMySettingPage: () => new MatreshkaMySettingPage(),
    Data: () => new Data(),
});

export { app, url }; 