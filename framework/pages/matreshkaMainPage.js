const MatreshkaMainPage = function(){ 

  const loginField = ('#login-input');
  const passwordField = ('[formcontrolname="password"]');
  const loginButton = ('[class="gl-btn mod-lg mod-purple gl-full-width"]');
  
  
  this.getUsername = async function (){
      const username = ('.user-name');
      return username;
  };

  
  this.login = async function (page, name, password){ 
    await page.fill(loginField, name);  

    await page.fill(passwordField, password); 
    
    await page.click(loginButton); 
  };



};

export { MatreshkaMainPage };