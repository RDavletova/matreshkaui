"use strict";

describe('Покупка билета.', function () {
  it('Пользователь может купить билет кошельком Матрешки', function _callee() {
    var email, password, ticketCount, commonCountTickets, GeneralCountTicketsText, countTicketsInModal, countTicketsInModalText;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return regeneratorRuntime.awrap(app().Data().emailForPayMatreshka);

          case 2:
            email = _context.sent;
            _context.next = 5;
            return regeneratorRuntime.awrap(app().Data().password);

          case 5:
            password = _context.sent;
            _context.next = 8;
            return regeneratorRuntime.awrap(app().Data().ticketCount);

          case 8:
            ticketCount = _context.sent;
            _context.next = 11;
            return regeneratorRuntime.awrap(app().MatreshkaMainPage().login(page, email, password));

          case 11:
            _context.next = 13;
            return regeneratorRuntime.awrap(app().MatreshkaMyTisketsPage().BuyTicketWithWallet(page, ticketCount));

          case 13:
            _context.next = 15;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getcommonCountTickets());

          case 15:
            commonCountTickets = _context.sent;
            _context.next = 18;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getElement(page, commonCountTickets));

          case 18:
            GeneralCountTicketsText = _context.sent;
            expect(String(GeneralCountTicketsText)).to.have.string(3 * ticketCount);
            _context.next = 22;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getcountTicketsInModal());

          case 22:
            countTicketsInModal = _context.sent;
            _context.next = 25;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getElement(page, countTicketsInModal));

          case 25:
            countTicketsInModalText = _context.sent;
            expect(countTicketsInModalText).to.have.string(3 * ticketCount);

          case 27:
          case "end":
            return _context.stop();
        }
      }
    });
  });
  it('Пользователь может купить билет расплачиваясь картой', function _callee2() {
    var email, password, ticketCount, priceenot, priceenotText;
    return regeneratorRuntime.async(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return regeneratorRuntime.awrap(app().Data().email);

          case 2:
            email = _context2.sent;
            _context2.next = 5;
            return regeneratorRuntime.awrap(app().Data().password);

          case 5:
            password = _context2.sent;
            _context2.next = 8;
            return regeneratorRuntime.awrap(app().Data().ticketCount);

          case 8:
            ticketCount = _context2.sent;
            _context2.next = 11;
            return regeneratorRuntime.awrap(app().MatreshkaMainPage().login(page, email, password));

          case 11:
            _context2.next = 13;
            return regeneratorRuntime.awrap(app().MatreshkaMyTisketsPage().BuyTicketWithCard(page, ticketCount));

          case 13:
            _context2.next = 15;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getPriceEnot());

          case 15:
            priceenot = _context2.sent;
            _context2.next = 18;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getElement(page, priceenot));

          case 18:
            priceenotText = _context2.sent;
            expect(priceenotText).to.have.string('45');

          case 20:
          case "end":
            return _context2.stop();
        }
      }
    });
  });
});