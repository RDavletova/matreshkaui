"use strict";

var _chai = _interopRequireDefault(require("chai"));

var _browser = require("../lib/browser/browser");

var _index = require("../framework/pages/index");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// для expect
var expect = _chai["default"].expect;
var page;
beforeEach(function _callee() {
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap((0, _browser.run)());

        case 2:
          _context.next = 4;
          return regeneratorRuntime.awrap((0, _browser["goto"])(_index.url));

        case 4:
          page = _context.sent;

        case 5:
        case "end":
          return _context.stop();
      }
    }
  });
});
afterEach(function _callee2() {
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap((0, _browser.stop)());

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  });
});
it('Пользователь может восстановить пароль', function _callee3() {
  var email, recoveryRasswordLabel, recoveryRasswordLabelText;
  return regeneratorRuntime.async(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return regeneratorRuntime.awrap((0, _index.app)().Data().email);

        case 2:
          email = _context3.sent;
          _context3.next = 5;
          return regeneratorRuntime.awrap((0, _index.app)().MatreshkaMySettingPage().RecoveryPassword(page, email));

        case 5:
          _context3.next = 7;
          return regeneratorRuntime.awrap((0, _index.app)().MatreshkaProfilePage().getPasswordRecoweryLabel());

        case 7:
          recoveryRasswordLabel = _context3.sent;
          _context3.next = 10;
          return regeneratorRuntime.awrap((0, _index.app)().MatreshkaProfilePage().getElement(page, recoveryRasswordLabel));

        case 10:
          recoveryRasswordLabelText = _context3.sent;
          expect(recoveryRasswordLabelText).to.have.string('Восстановление пароля');

        case 12:
        case "end":
          return _context3.stop();
      }
    }
  });
});