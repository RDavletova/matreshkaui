"use strict";

describe('Мои билеты', function () {
  it('Пользователь может найти билет по номеру билета', function _callee() {
    var email, password, ticketNumber, ticketNumberElem, ticketNumberElemText;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return regeneratorRuntime.awrap(app().Data().email);

          case 2:
            email = _context.sent;
            _context.next = 5;
            return regeneratorRuntime.awrap(app().Data().password);

          case 5:
            password = _context.sent;
            _context.next = 8;
            return regeneratorRuntime.awrap(app().MatreshkaMainPage().login(page, email, password));

          case 8:
            _context.next = 10;
            return regeneratorRuntime.awrap(app().Data().ticketNumber);

          case 10:
            ticketNumber = _context.sent;
            _context.next = 13;
            return regeneratorRuntime.awrap(app().MatreshkaMyTisketsPage().SearchByTicketNumber(page, ticketNumber));

          case 13:
            _context.next = 15;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getTicketNumberElem());

          case 15:
            ticketNumberElem = _context.sent;
            _context.next = 18;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getElement(page, ticketNumberElem));

          case 18:
            ticketNumberElemText = _context.sent;
            expect(ticketNumberElemText).to.have.string('№ ' + ticketNumber);

          case 20:
          case "end":
            return _context.stop();
        }
      }
    });
  });
  it('Пользователь может отфильтровать билеты по выигрышным билетам', function _callee2() {
    var email, password, winnerTicketNumberElem, winnerTicketNumberElemText;
    return regeneratorRuntime.async(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return regeneratorRuntime.awrap(app().Data().email);

          case 2:
            email = _context2.sent;
            _context2.next = 5;
            return regeneratorRuntime.awrap(app().Data().password);

          case 5:
            password = _context2.sent;
            _context2.next = 8;
            return regeneratorRuntime.awrap(app().MatreshkaMainPage().login(page, email, password));

          case 8:
            _context2.next = 10;
            return regeneratorRuntime.awrap(app().MatreshkaMyTisketsPage().FilterByTicketWinner(page));

          case 10:
            _context2.next = 12;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getTicketNumberElem());

          case 12:
            winnerTicketNumberElem = _context2.sent;
            _context2.next = 15;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getElement(page, winnerTicketNumberElem));

          case 15:
            winnerTicketNumberElemText = _context2.sent;
            expect(winnerTicketNumberElemText).to.have.string('№ 89856858');

          case 17:
          case "end":
            return _context2.stop();
        }
      }
    });
  });
});