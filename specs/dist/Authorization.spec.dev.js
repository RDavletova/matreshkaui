"use strict";

it('Авторизация пользователя', function _callee() {
  var email, password, profileName, profileNameText;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(app().Data().email);

        case 2:
          email = _context.sent;
          _context.next = 5;
          return regeneratorRuntime.awrap(app().Data().password);

        case 5:
          password = _context.sent;
          _context.next = 8;
          return regeneratorRuntime.awrap(app().MatreshkaMainPage().login(page, email, password));

        case 8:
          _context.next = 10;
          return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getUsername());

        case 10:
          profileName = _context.sent;
          _context.next = 13;
          return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getElement(page, profileName));

        case 13:
          profileNameText = _context.sent;
          expect(profileNameText).to.have.string(' Алевтина Иванова');

        case 15:
        case "end":
          return _context.stop();
      }
    }
  });
});