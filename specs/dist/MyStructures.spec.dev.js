"use strict";

describe('Моя структура.', function () {
  it('Пользователь может найти реферала по имени ', function _callee() {
    var email, password, username, refferalName, refferalNameText;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return regeneratorRuntime.awrap(app().Data().email);

          case 2:
            email = _context.sent;
            _context.next = 5;
            return regeneratorRuntime.awrap(app().Data().password);

          case 5:
            password = _context.sent;
            _context.next = 8;
            return regeneratorRuntime.awrap(app().MatreshkaMainPage().login(page, email, password));

          case 8:
            _context.next = 10;
            return regeneratorRuntime.awrap(app().Data().username);

          case 10:
            username = _context.sent;
            _context.next = 13;
            return regeneratorRuntime.awrap(app().MatreshkaMyStructurePage().searchByNameRefferal(page, username));

          case 13:
            _context.next = 15;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getRefferalName());

          case 15:
            refferalName = _context.sent;
            _context.next = 18;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getElement(page, refferalName));

          case 18:
            refferalNameText = _context.sent;
            expect(refferalNameText).to.have.string(username);

          case 20:
          case "end":
            return _context.stop();
        }
      }
    });
  });
  it('Пользователь может найти реферала по номеру телефона', function _callee2() {
    var email, password, phone, refferalPhone, refferalPhoneText;
    return regeneratorRuntime.async(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return regeneratorRuntime.awrap(app().Data().email);

          case 2:
            email = _context2.sent;
            _context2.next = 5;
            return regeneratorRuntime.awrap(app().Data().password);

          case 5:
            password = _context2.sent;
            _context2.next = 8;
            return regeneratorRuntime.awrap(app().MatreshkaMainPage().login(page, email, password));

          case 8:
            _context2.next = 10;
            return regeneratorRuntime.awrap(app().Data().phone);

          case 10:
            phone = _context2.sent;
            _context2.next = 13;
            return regeneratorRuntime.awrap(app().MatreshkaMyStructurePage().searchByPhoneRefferal(page, phone));

          case 13:
            _context2.next = 15;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getRefferalPhone());

          case 15:
            refferalPhone = _context2.sent;
            _context2.next = 18;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getElement(page, refferalPhone));

          case 18:
            refferalPhoneText = _context2.sent;
            expect(refferalPhoneText).to.have.string(phone);

          case 20:
          case "end":
            return _context2.stop();
        }
      }
    });
  });
  it('Пользователь может найти реферала по мейлу', function _callee3() {
    var email, password, emailRefferal, refferalEmail, refferalEmailText;
    return regeneratorRuntime.async(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return regeneratorRuntime.awrap(app().Data().email);

          case 2:
            email = _context3.sent;
            _context3.next = 5;
            return regeneratorRuntime.awrap(app().Data().password);

          case 5:
            password = _context3.sent;
            _context3.next = 8;
            return regeneratorRuntime.awrap(app().MatreshkaMainPage().login(page, email, password));

          case 8:
            _context3.next = 10;
            return regeneratorRuntime.awrap(app().Data().emailRefferal);

          case 10:
            emailRefferal = _context3.sent;
            _context3.next = 13;
            return regeneratorRuntime.awrap(app().MatreshkaMyStructurePage().searchByEmailRefferal(page, emailRefferal));

          case 13:
            _context3.next = 15;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getRefferalEmail());

          case 15:
            refferalEmail = _context3.sent;
            _context3.next = 18;
            return regeneratorRuntime.awrap(app().MatreshkaProfilePage().getElement(page, refferalEmail));

          case 18:
            refferalEmailText = _context3.sent;
            expect(refferalEmailText).to.have.string(emailRefferal);

          case 20:
          case "end":
            return _context3.stop();
        }
      }
    });
  });
});