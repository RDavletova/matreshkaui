import chai from 'chai'; // для expect
import { goto, run, stop } from '../lib/browser/browser';
import { url, app } from '../framework/pages/index';


const { expect } = chai;

let page;



beforeEach(async () => {
  await run();
  page = await goto(url); 
});

afterEach(async () => {
  await stop();
});

describe('Моя структура.', () => {
  it('Пользователь может найти реферала по имени ', async () => { 

    const email = await app().Data().email;

    const password = await app().Data().password;

    await app().MatreshkaMainPage().login(page, email, password);

    const username = await app().Data().username;

    await app().MatreshkaMyStructurePage().searchByNameRefferal(page, username);

    const refferalName = await app().MatreshkaMyStructurePage().getRefferalName();

    const refferalNameText = await app().MatreshkaProfilePage().getElement(page, refferalName);

    expect(refferalNameText)
      .to
      .have
      .string(username);

  });



  it('Пользователь может найти реферала по номеру телефона', async () => { 

    const email = await app().Data().email;

    const password = await app().Data().password;

    await app().MatreshkaMainPage().login(page, email, password);

    const phone = await app().Data().phone;

    await app().MatreshkaMyStructurePage().searchByPhoneRefferal(page, phone);

    const refferalPhone = await app().MatreshkaMyStructurePage().getRefferalPhone();

    const refferalPhoneText = await app().MatreshkaProfilePage().getElement(page, refferalPhone);

    expect(refferalPhoneText)
      .to
      .have
      .string(phone);

  });

  it('Пользователь может найти реферала по мейлу', async () => { 
    const email = await app().Data().email;

    const password = await app().Data().password;

    await app().MatreshkaMainPage().login(page, email, password);

    const emailRefferal = await app().Data().emailRefferal;

    await app().MatreshkaMyStructurePage().searchByEmailRefferal(page, emailRefferal);

    const refferalEmail = await app().MatreshkaMyStructurePage().getRefferalEmail();

    const refferalEmailText = await app().MatreshkaProfilePage().getElement(page, refferalEmail);
    

    expect(refferalEmailText)
      .to
      .have
      .string(emailRefferal);

  });

});
