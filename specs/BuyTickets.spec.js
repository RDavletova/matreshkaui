import chai from 'chai'; // для expect
import { goto, run, stop } from '../lib/browser/browser';
import { url, app } from '../framework/pages/index';

let page;



beforeEach(async () => {
  await run();
  page = await goto(url); 
});

afterEach(async () => {
  await stop();
});



describe('Покупка билета.', () => {
  it.skip('Пользователь может купить билет кошельком Матрешки', async () => {

    const email = await app().Data().emailForPayMatreshka;

    const password = await app().Data().password;
    
    const ticketCount = await app().Data().ticketCount; 

    await app().MatreshkaMainPage().login(page, email, password);

    await app().MatreshkaMyTisketsPage().BuyTicketWithWallet(page, ticketCount);

    const commonCountTickets = await app().MatreshkaMyTisketsPage().getcommonCountTickets();

    const GeneralCountTicketsText = await app().MatreshkaProfilePage().getElement(page, commonCountTickets);

    expect(String(GeneralCountTicketsText))
      .to
      .have
      .string(3 * ticketCount);

    
    const countTicketsInModal = await app().MatreshkaMyTisketsPage().getcountTicketsInModal();

    const countTicketsInModalText = await app().MatreshkaProfilePage().getElement(page, countTicketsInModal);

    expect(countTicketsInModalText)
      .to
      .have
      .string(3 * ticketCount);
  });


  it('Пользователь может купить билет расплачиваясь картой', async () => {
    
    const email = await app().Data().email;

    const password = await app().Data().password;
    
    const ticketCount = await app().Data().ticketCount; 

    await app().MatreshkaMainPage().login(page, email, password);

    await app().MatreshkaMyTisketsPage().BuyTicketWithCard(page, ticketCount); 

    const priceenot = await app().MatreshkaMyTisketsPage().getPriceEnot();

    const priceenotText = await app().MatreshkaProfilePage().getElement(page, priceenot);

    expect(priceenotText)
      .to
      .have
      .string('45');
  });

});
