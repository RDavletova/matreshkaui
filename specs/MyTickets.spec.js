import chai from 'chai'; // для expect
import { goto, run, stop } from '../lib/browser/browser';
import { url, app} from '../framework/pages/index';

const { expect } = chai;

let page;



beforeEach(async () => {
  await run();
  page = await goto(url); 
});

afterEach(async () => {
  await stop();
});





describe('Мои билеты', () => {

  it('Пользователь может найти билет по номеру билета', async () => { 

    const email = await app().Data().email; 
    
    const password = await app().Data().password;

    await app().MatreshkaMainPage().login(page, email, password);

    const ticketNumber = await app().Data().ticketNumber;

    await app().MatreshkaMyTisketsPage().SearchByTicketNumber(page, ticketNumber);

    const ticketNumberElem = await app().MatreshkaMyTisketsPage().getTicketNumberElem();

    const ticketNumberElemText = await app().MatreshkaProfilePage().getElement(page, ticketNumberElem);

    expect(ticketNumberElemText)
      .to
      .have
      .string('№ ' + ticketNumber);
  });


  it('Пользователь может отфильтровать билеты по выигрышным билетам', async () => { 

    const email = await app().Data().email; 

    const password = await app().Data().password;

    await app().MatreshkaMainPage().login(page, email, password);

    await app().MatreshkaMyTisketsPage().FilterByTicketWinner(page);

    const winnerTicketNumberElem = await app().MatreshkaMyTisketsPage().getTicketNumberElem(); 

    const winnerTicketNumberElemText = await app().MatreshkaProfilePage().getElement(page, winnerTicketNumberElem);

    expect(winnerTicketNumberElemText)
      .to
      .have
      .string('№ 89856858'); 
  });

});
