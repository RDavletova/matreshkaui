import { app, url } from '../framework/pages/index';
import chai from 'chai'; // для expect
import { goto, run, stop } from '../lib/browser/browser';

const { expect } = chai;
let page;

beforeEach(async () => {
  await run();
  page = await goto(url); 
});

afterEach(async () => {
  await stop();
});

it('Авторизация пользователя', async () => {

    const email = await app().Data().email; 

    const password = await app().Data().password;

    await app().MatreshkaMainPage().login(page, email, password);

    const profileName = await app().MatreshkaMainPage().getUsername();

    const profileNameText = await app().MatreshkaProfilePage().getElement(page, profileName);

    expect(profileNameText)
      .to
      .have
      .string(' Алевтина Иванова');
});


it('Пользователь может восстановить пароль', async () => { 

    const email = await app().Data().email;

    await app().MatreshkaMySettingPage().recoveryPassword(page, email);

    const recoveryRasswordLabel = await app().MatreshkaProfilePage().getPasswordRecoweryLabel(); 

    const recoveryRasswordLabelText = await app().MatreshkaProfilePage().getElement(page, recoveryRasswordLabel);

    expect(recoveryRasswordLabelText)
      .to
      .have
      .string('Восстановление пароля');

});

it('Пользователь может изменить пароль', async () => { 

    const password = await app().Data().password;
    const newPassword = await app().Data().newPassword;

    await app().MatreshkaMySettingPage().updatePassword(page, password, newPassword);

    const userName = await app().MatreshkaMainPage().getUsername(); 

    const userNameText = await app().MatreshkaProfilePage().getElement(page, userName);

    expect(userNameText)
      .to
      .have
      .string(' Алевтина Иванова');

});
